# BEAM POC

Este repositorio contiene POC con Apache Beam. Esta POC consiste en escribir datos desde un productor en un tema/tópico de Kafka, leerlos desde un consumidor y verificar si estos datos corresponden a un cliente consultando en Elasticsearch. Si no corresponden a un cliente, se escribirán en un tema/tópico de "desconocidos". Si corresponden a un cliente, se enriquecerán los datos y se escribirán en un tema/tópico de "conocidos".

### Requerimientos
- Java 7
- Maven
- Apache Beam
- Apache Kafka
- Elasticsearch

### Pasos

Antes de ejecutar la aplicación, es necesario tener lanzados Zookeeper y Kafka.
~~~
$ ./bin/zookeeper-server-start.sh ./config/zookeeper.properties

$ ./bin/kafka-server-start.sh ./config/server.properties
~~~

Será necesario tener creados los temas/tópicos de Kafka en los que se va a escribir/leer.
~~~
$./bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic input

$./bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic customer

$./bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic unknown
~~~

Ejecutamos un consumidor en un cliente de línea de comandos para poder leer los datos enviados a Kafka por el productor.
~~~
$ ./bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test-rendimiento --from-beginning
~~~

Para más información sobre Kafka: https://kafka.apache.org/quickstart

Insertamos los datos de un cliente en Elasticsearch. Será necesario tener instalado y lanzado Elasticsearch. Para más información: https://www.elastic.co/guide/en/elasticsearch/reference/current/_installation.html
~~~
curl -X PUT http://localhost:9200/wallmart/test/1 -d '{ "name": "Pilar", "age": 57 }'
~~~

Y finalmente, ejecutamos la aplicación.
~~~
$ cd KafkaConsumer
$ mvn clean package
$ mvn exec:java -Dexec.mainClass="main.Main"
~~~

Para comprobar qué datos se han escrito en los temas/tópicos de "conocidos" y "desconocidos", ejecutamos dos consumidores en dos clientes de línea de comandos, uno por consumidor.
~~~
$ ./bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic customer --from-beginning

$ ./bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic unknown --from-beginning
~~~

### Referencias
- Apache Beam Documentation - https://beam.apache.org/documentation/

