package db;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import util.GenerateCurrentDateTime;

import java.io.BufferedReader;
import java.io.InputStreamReader;


public class ElasticDAO {

    private static final Logger LOGGER = LogManager.getLogger();

    public String readDoc(String url) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet getRequest = new HttpGet("http://localhost:9200" + url + "?pretty=true");
        String entityStr = "";
        HttpResponse response = null;
        try {
            response = httpClient.execute(getRequest);
            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));
            while (br.readLine() != null) {
                if (entityStr.equals("")) entityStr = "{";
                entityStr += br.readLine();
            }
            System.out.println(entityStr);
        } catch (Exception e) {
            String currDate = new GenerateCurrentDateTime().getDateTimeStr();
            LOGGER.error(currDate + "[db.db.ElasticDAO][readDoc] An error occurred while trying to read the document");
            e.printStackTrace();
        } finally {
            String currDate = new GenerateCurrentDateTime().getDateTimeStr();
            LOGGER.info(currDate + "[db.db.ElasticDAO][readDoc] " + response.toString());
            return entityStr;
        }
    }
}