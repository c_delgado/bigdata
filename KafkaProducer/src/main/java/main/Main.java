package main;

import com.google.common.io.Resources;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;


public class Main {

    public static void main(String[] args) throws IOException {
        if (args.length != 4) {
            System.out.println("Please provide command line arguments: startMsg, endMsg, nDiv, dataSize");
            System.exit(-1);
        }

        KafkaProducer<String, String> producer = null;
        try {
            InputStream props = Resources.getResource("producer.props").openStream();
            Properties properties = new Properties();
            properties.load(props);
            producer = new KafkaProducer<String, String>(properties);

            int startMsg = Integer.parseInt(args[0]);
            int endMsg = Integer.parseInt(args[1]);
            int nDiv = Integer.parseInt(args[2]);
            int dataSize = Integer.parseInt(args[3]);

            Long startCreation = new Date().getTime();
            System.out.println("Inicio creación mensajes: " + Long.toString(startCreation));

            ArrayList<JSONObject> messages = new ArrayList<JSONObject>();
            SecureRandom random = new SecureRandom();
            for (int i = startMsg; i <= endMsg; i++) {
                String data = new BigInteger(dataSize, random).toString();
                JSONObject message = new JSONObject();
                message.put("productId", Integer.toString(i%nDiv));
                message.put("dataIndex", i);
                message.put("data", data);
                messages.add(message);
            }

            Long endCreation = new Date().getTime();
            System.out.println("Fin creación mensajes: " + Long.toString(endCreation));
            Long totalCreation = endCreation - startCreation;
            System.out.println("La creación ha tardado " + totalCreation + " segundos");

            Long startSending = new Date().getTime();
            System.out.println("Inicio envío mensajes: " + Long.toString(startCreation));

            for (int i = startMsg; i <= endMsg; i++) {
                producer.send(new ProducerRecord<String, String>("test-rendimiento",
                        messages.get(i - startMsg).toString()));
            }

            Long endSending = new Date().getTime();
            System.out.println("Fin envío mensajes: " + Long.toString(endCreation));
            Long totalSending = endSending - startSending;
            System.out.println("El envío ha tardado " + totalSending + " segundos");
        } catch (Throwable throwable) {
            System.out.printf("%s", throwable.getStackTrace());
        } finally {
            producer.close();
        }
    }
}
