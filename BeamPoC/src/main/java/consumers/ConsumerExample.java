package consumers;

import org.apache.beam.runners.flink.FlinkPipelineOptions;
import org.apache.beam.runners.flink.FlinkRunner;
import org.apache.beam.runners.flink.translation.wrappers.streaming.io.UnboundedFlinkSource;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.coders.StringUtf8Coder;
import org.apache.beam.sdk.io.Read;
import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer08;
import org.apache.flink.streaming.util.serialization.SimpleStringSchema;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import java.util.Properties;
import db.ElasticDAO;
import producers.ProducerPostCustomerExample;
import producers.ProducerPostUnknownExample;


public class ConsumerExample {

    private static final String KAFKA_TOPIC = "input";  // Default kafka topic to read from
    private static final String KAFKA_AVRO_TOPIC = "output";  // Default kafka topic to read from
    private static final String KAFKA_BROKER = "localhost:9092";  // Default kafka broker to contact
    private static final String GROUP_ID = "myGroup";  // Default groupId
    private static final String ZOOKEEPER = "localhost:2181";  // Default zookeeper to connect (Kafka)

    public void receiveMessages() {
        Pipeline p = initializePipeline(new String[]{});
        KafkaConsumerOptions options = getOptions(p);
        options.setKafkaTopic("input");

        FlinkKafkaConsumer08<String> kafkaConsumer =
                new FlinkKafkaConsumer08<String>(options.getKafkaTopic(),
                        new SimpleStringSchema(), getKafkaProps(options));

        p
                .apply(Read.from(UnboundedFlinkSource.of(kafkaConsumer))).setCoder(StringUtf8Coder.of())
                .apply(ParDo.of(new ProcessDataFn<String>()));

        p.run();
    }

    private static class ProcessDataFn<T> extends DoFn<T, T> {

        @ProcessElement
        public void processElement(ProcessContext c) throws Exception {
            JSONParser parser = new JSONParser();
            JSONObject dataJson = (JSONObject) parser.parse((String) c.element());

            ElasticDAO dao = new ElasticDAO();
            String doc = dao.readDoc("/wallmart/test/" + dataJson.get("id").toString());
            JSONObject docJson = (JSONObject) parser.parse(doc);

            if (docJson.get("found").toString().equals("true")) {
                String dataStr = docJson.get("data").toString();
                docJson.put("isCustomer", "true");

                ProducerPostCustomerExample producer = new ProducerPostCustomerExample();
                producer.sendMessages(new String[]{docJson.toString()});
            } else {
                ProducerPostUnknownExample producer = new ProducerPostUnknownExample();
                producer.sendMessages(new String[]{doc});
            }
        }
    }

    public interface KafkaConsumerOptions extends FlinkPipelineOptions {
        @Description("The Kafka topic to read from")
        @Default.String(KAFKA_TOPIC)
        String getKafkaTopic();

        void setKafkaTopic(String value);

        void setKafkaAvroTopic(String value);

        @Description("The Kafka topic to read from")
        @Default.String(KAFKA_AVRO_TOPIC)
        String getKafkaAvroTopic();

        @Description("The Kafka Broker to read from")
        @Default.String(KAFKA_BROKER)
        String getBroker();

        void setBroker(String value);

        @Description("The Zookeeper server to connect to")
        @Default.String(ZOOKEEPER)
        String getZookeeper();

        void setZookeeper(String value);

        @Description("The groupId")
        @Default.String(GROUP_ID)
        String getGroup();

        void setGroup(String value);
    }

    private static Pipeline initializePipeline(String[] args) {
        KafkaConsumerOptions options =
                PipelineOptionsFactory.fromArgs(args).as(KafkaConsumerOptions.class);
        options.setStreaming(true);
        options.setRunner(FlinkRunner.class);
        options.setCheckpointingInterval(1000L);
        options.setNumberOfExecutionRetries(5);
        options.setExecutionRetryDelay(3000L);
        return Pipeline.create(options);
    }

    private static KafkaConsumerOptions getOptions(Pipeline p) {
        return p.getOptions().as(KafkaConsumerOptions.class);
    }

    private static Properties getKafkaProps(KafkaConsumerOptions options) {
        Properties props = new Properties();
        props.setProperty("zookeeper.connect", options.getZookeeper());
        props.setProperty("bootstrap.servers", options.getBroker());
        props.setProperty("group.id", options.getGroup());
        return props;
    }
}
