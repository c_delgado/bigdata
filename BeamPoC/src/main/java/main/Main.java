package main;

import consumers.ConsumerExample;
import org.json.simple.JSONObject;


public class Main {

    public static void main(String[] args) {
        producers.ProducerPreExample producer = new producers.ProducerPreExample();
        JSONObject data1 = new JSONObject();
        JSONObject data2 = new JSONObject();

        data1.put("id", 1);
        data2.put("id", 2);

        String[] messages = new String[]{data1.toString(), data2.toString()};
        producer.sendMessages(messages);

        ConsumerExample consumer = new ConsumerExample();
        consumer.receiveMessages();
    }
}
