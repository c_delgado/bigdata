# DOCKER

### Apache Kafka

La imagen de Docker que he utilizado para el despliegue de Apache Kafka es:
https://hub.docker.com/r/wurstmeister/kafka/

Se ha modificado el docker-compose original de forma que se lancen dos brokers de Kafka.
~~~
version: '2'
services:
  zookeeper:
    image: wurstmeister/zookeeper
    ports:
      - "2181:2181"
  kafka1:
    image: wurstmeister/kafka
    ports:
      - "9092:9092"
    environment:
      KAFKA_ADVERTISED_HOST_NAME: 52.3.247.253
      KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
  kafka2:
    image: wurstmeister/kafka
    ports:
      - "9093:9092"
    environment:
      KAFKA_ADVERTISED_HOST_NAME: 52.3.247.253
      KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
~~~

El comando para lanzar Kafka es:
~~~
docker-compose up -d
~~~

### Elasticsearch/Kibana

Las imágenes de Docker que he utilizado, tanto para el despliegue de Elasticsearch como el de Kibana, son las oficiales.
https://hub.docker.com/_/elasticsearch/
https://hub.docker.com/_/kibana/

*Nota: La última vez que he entrado en la información del repositorio de la imagen de Kibana ponía que estaba obsoleta.*

Los comandos concretos para lanzar Elasticsearch/Kibana son:
~~~
docker run -p 9200:9200 -d --name es elasticsearch
docker run --link es:elasticsearch -p 5601:5601 -d kibana
~~~

### Grafana

La imagen de Docker que he utilizado para el despliegue de Grafana es:
https://hub.docker.com/r/grafana/grafana/

*Nota: En el momento de escribir esta documentación es necesario utilizar la versión 4.1.0-beta1 de Grafana si se desea integrar con la última versión (5.2) de Elasticsearch.*

El comando para lanzar Grafana es:
~~~
docker run -i -p 3000:3000 grafana/grafana:4.1.0-beta1
~~~
