# KAFKA PRODUCER

Este repositorio contiene el código en Java de un productor de Kafka para pruebas de la POC Farm2Fork. Este productor genera y escribe datos en Kafka en función de los argumentos. Su objetivo es poner a prueba el rendimiento de Kafka.

### Requerimientos
- Java 8
- Maven
- Apache Kafka

### Estructura de los datos

Los datos que escribe en Kafka tienen formato JSON, con el siguiente esquema:
~~~
{"productId":00000001, "dataIndex":001, "data":"Estos son los datos"}
~~~

El elemento `productId` es el identificador del producto.
El elemento `dataIndex` es el identificador de los datos del evento asociado a un producto.
El elemento `data` corresponde con los datos del evento.

### Pasos

Antes de ejecutar la aplicación, es necesario tener lanzados Zookeeper y Kafka.
~~~
$ ./bin/zookeeper-server-start.sh ./config/zookeeper.properties

$ ./bin/kafka-server-start.sh ./config/server.properties
~~~

Será necesario tener creado el tema de Kafka del que nuestro consumidor va a leer.
~~~
$./bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test-rendimiento
~~~

Ejecutamos un consumidor en un cliente de línea de comandos para poder leer los datos enviados a Kafka por el productor.
~~~
$ ./bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test-rendimiento --from-beginning
~~~

Para más información sobre Kafka: https://kafka.apache.org/quickstart

Y finalmente, ejecutamos el productor.
~~~
$ cd KafkaProducer
$ mvn clean package
$ mvn exec:java -Dexec.mainClass="main.Consumer" -Dexec.args="0 1000 3 140"
~~~
Los argumentos son:
~~~
startMsg endMsg nDiv dataSize
~~~
- `startMsg`: El comienzo del rango de mensajes generados.
- `endMsg`: El final del rango de mensajes generados.
- `nDiv`: El número de productos a los que se asignan los distintos mensajes generados.
- `dataSize`: El tamaño del `string` del valor del elemento `data` del mensaje.
