# FORMACIÓN

### CURSOS OFICIALES

- MongoDB: **https://university.mongodb.com/courses/catalog**
- Cassandra: **https://academy.datastax.com/courses**
- Neo4j: **https://neo4j.com/graphacademy/**

### TUTORIALES

- Flink: **http://dataartisans.github.io/flink-training/**
- Hadoop Tutorial (Hortonworks): **https://es.hortonworks.com/hadoop-tutorial/hello-world-an-introduction-to-hadoop-hcatalog-hive-and-pig/**

### PLATAFORMAS

- Coursera: **https://www.coursera.org/**
- Udacity: **https://www.udacity.com/**
- EdX: **https://www.edx.org/**
- Big Data University: **https://bigdatauniversity.com/**
- MiriadaX: **https://miriadax.net/home**
- Udemy: **https://www.udemy.com/**

### ARTÍCULOS

- Apache Kafka Quickstart: **https://kafka.apache.org/quickstart**
- Apache Kafka Consumer 0.9: **https://www.confluent.io/blog/tutorial-getting-started-with-the-new-apache-kafka-0-9-consumer-client/**


