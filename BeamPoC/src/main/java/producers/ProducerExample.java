package producers;

import org.apache.beam.runners.flink.FlinkPipelineOptions;
import org.apache.beam.runners.flink.FlinkRunner;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptionsFactory;

import java.util.Properties;


public class ProducerExample {

    private static final String KAFKA_TOPIC = "input";  // Default kafka topic to read from
    private static final String KAFKA_AVRO_TOPIC = "output";  // Default kafka topic to read from
    private static final String KAFKA_BROKER = "localhost:9092";  // Default kafka broker to contact
    private static final String GROUP_ID = "myGroup";  // Default groupId
    private static final String ZOOKEEPER = "localhost:2181";  // Default zookeeper to connect (Kafka)

    public interface KafkaProducerOptions extends FlinkPipelineOptions {
        @Description("The Kafka topic to read from")
        @Default.String(KAFKA_TOPIC)
        String getKafkaTopic();

        void setKafkaTopic(String value);

        void setKafkaAvroTopic(String value);

        @Description("The Kafka topic to read from")
        @Default.String(KAFKA_AVRO_TOPIC)
        String getKafkaAvroTopic();

        @Description("The Kafka Broker to read from")
        @Default.String(KAFKA_BROKER)
        String getBroker();

        void setBroker(String value);

        @Description("The Zookeeper server to connect to")
        @Default.String(ZOOKEEPER)
        String getZookeeper();

        void setZookeeper(String value);

        @Description("The groupId")
        @Default.String(GROUP_ID)
        String getGroup();

        void setGroup(String value);
    }

    protected static Pipeline initializePipeline(String[] args) {
        KafkaProducerOptions options =
                PipelineOptionsFactory.fromArgs(args).as(KafkaProducerOptions.class);
        options.setStreaming(true);
        options.setRunner(FlinkRunner.class);
        options.setCheckpointingInterval(1000L);
        options.setNumberOfExecutionRetries(5);
        options.setExecutionRetryDelay(3000L);
        return Pipeline.create(options);
    }

    protected static KafkaProducerOptions getOptions(Pipeline p) {
        return p.getOptions().as(KafkaProducerOptions.class);
    }

    protected static Properties getKafkaProps(KafkaProducerOptions options) {
        Properties props = new Properties();
        props.setProperty("zookeeper.connect", options.getZookeeper());
        props.setProperty("bootstrap.servers", options.getBroker());
        props.setProperty("group.id", options.getGroup());
        return props;
    }
}
