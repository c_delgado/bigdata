package util;

import org.joda.time.DateTime;

public class GenerateCurrentDateTime {

    private DateTime date;

    public GenerateCurrentDateTime() {
        this.date = new DateTime();
    }

    public String getDateTimeStr() {
        String year = String.valueOf(this.date.getYear());
        String month = String.valueOf(this.date.getMonthOfYear());
        String day = String.valueOf(this.date.getDayOfMonth());
        String hour = String.valueOf(this.date.getHourOfDay());
        String mins = String.valueOf(this.date.getMinuteOfHour());
        String secs = String.valueOf(this.date.getSecondOfMinute());

        return "[" + year + "-" + month + "-" + day + " " + hour + ":" + mins + ":" + secs + "] ";
    }
}
