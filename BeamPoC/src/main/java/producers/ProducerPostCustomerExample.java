package producers;

import org.apache.beam.runners.flink.translation.wrappers.streaming.io.UnboundedFlinkSink;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.Write;
import org.apache.beam.sdk.transforms.Create;
import org.apache.beam.sdk.values.PCollection;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer08;
import org.apache.flink.streaming.util.serialization.SimpleStringSchema;


public class ProducerPostCustomerExample extends ProducerExample {

    public void sendMessages(String[] messages) {
        Pipeline p = initializePipeline(new String[]{});
        KafkaProducerOptions options = getOptions(p);
        options.setKafkaTopic("customer");

        PCollection<String> words = p.apply(Create.of(messages));

        FlinkKafkaProducer08<String> kafkaSink =
                new FlinkKafkaProducer08<String>(options.getKafkaTopic(),
                        new SimpleStringSchema(), getKafkaProps(options));

        words.apply(Write.to(UnboundedFlinkSink.of(kafkaSink)));

        p.run();
    }
}
